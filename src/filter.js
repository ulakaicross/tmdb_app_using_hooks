import React from "react";

const Filter = ({ ...props }) => {

  const defaultValue = 3;
  const items = [];

  for (let i = 9; i >= 0; i = i - 0.5) {
    if (i === defaultValue) {
      items.push(
        <option value={defaultValue} key={i}>
          {i}
        </option>
      );
    } else {
      items.push(
        <option value={i} key={i}>
          {i}
        </option>
      );
    }
  }

  return (
    <form className="frm frm-header" onChange={props.onChange}>
      <label className="lbl">Sort by rating &nbsp;</label>
      <select defaultValue={defaultValue} className="slct">
        {items}
      </select>
      <span className="lbl-spn"> and above</span>
    </form>
  );
};

export default Filter;