import React from "react";

class Card extends React.PureComponent {

  addGenres = genreId => {
    let result = this.props.genreList.filter(obj => {
      return obj.id === genreId;
    });
    return result[0].name;
  };

  render() {
    const { src, title, overview, rating, genres } = this.props;
    return (
      <div className="card-container col-xs-12 col-lg-6 col-xl-4">
        <div className="card-item row">
          <div className="image-container col-xs-12 col-lg-6">
            <img className="img img-card" src={src} alt="" />
          </div>
          <div className="info-container col-xs-12 col-lg-6">
            <h2>{title}</h2>
            <h3>Synopsis:</h3>
            <p>{overview}</p>
            <h3>Rating: {rating} / 10</h3>
            <h3>
              Genres:
              <br />
              {genres.map(item => {
                return (
                  <span key={item} className="info-spn">
                    {this.addGenres(item)}
                  </span>
                );
              })}
            </h3>
          </div>
        </div>
      </div>
    );
  }
}

export default Card;