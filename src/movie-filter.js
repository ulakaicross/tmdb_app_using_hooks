import React, { useState, useEffect } from "react";
import Filter from "./filter";
import Card from "./card";

const apiKey = "ee25b2dc7b39cf02eb4b0848345bd05a";
const imagePath = "https://image.tmdb.org/t/p/original";
const defaultFilterValue = "3";

const HookMovieFilter = () => {
  const [filterValue, setFilterValue] = useState(defaultFilterValue);
  const [movies, setMovies] = useState({});
  const [genre, setGenre] = useState({});
  const [loaded1, setLoaded1] = useState(false);
  const [loaded2, setLoaded2] = useState(false);
  const [moviesList, setMoviesList] = useState({});

  const fetchData = async (url, flag) => {
    try {
      const result = await fetch(url);
      const json = await result.json();
      switch (flag) {
        case 0:
          setMovies(json.results);
          break;
        case 1:
          setGenre(json.genres);
          break;
        default:
          console.warn("switch defaulted");
      }
    } catch (e) {
      console.warn(e);
    }
  };

  useEffect(() => {
    fetchData(
      "https://api.themoviedb.org/3/movie/now_playing?api_key=" +
        apiKey +
        "&language=en-US&page=1",
      0
    ).then(() => {
      setLoaded1(true);
    });
    fetchData(
      "https://api.themoviedb.org/3/genre/movie/list?api_key=" +
        apiKey +
        "&language=en-US/",
      1
    ).then(() => {
      setLoaded2(true);
    });
  }, []);

  useEffect(() => {
    if (movies.length) {
      setMoviesList(() => {
        let filtered = movies
          .filter(result => result.vote_average >= filterValue)
          .sort((a, b) => b.vote_average - a.vote_average);
        return filtered;
      });
    }
  }, [filterValue, movies]);

  return (
    <div className="app">
      <header className="hdr">
        <h1>Out Now!</h1>
      </header>
      <Filter
        onChange={e => {
          setFilterValue(e.target.value);
        }}
      />
      <div className="film-card col-xs-12">
        <div className="row">
          {loaded1 && loaded2 ? (
            moviesList.map(movie => (
              <Card
                key={movie.id}
                src={imagePath + movie.poster_path}
                movieTitle={movie.title}
                overview={movie.overview}
                rating={movie.vote_average}
                genres={movie.genre_ids}
                genreList={genre}
              />
            ))
          ) : (
            <p>Loading...</p>
          )}
        </div>
      </div>
    </div>
  );
};

export default HookMovieFilter;
